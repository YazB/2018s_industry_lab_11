package ictgradschool.industry.lab.swing.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements  ActionListener {

    JTextField one;
    JTextField two;
    JButton add;
    JButton subtract;
    JLabel result;
    JTextField showResult;


    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        one = new JTextField(10);
        two = new JTextField(10);
        add = new JButton("Add");
        subtract = new JButton("Subtract");
        showResult = new JTextField(20);

        JLabel result;
        result = new JLabel("Result:");

        add(one);
        add(two);
        add(add);
        add(subtract);
        add(result);
        add(showResult);

        add.addActionListener(this);
        subtract.addActionListener(this);

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        /**
         * New method here???
         */
        double result = 0.0;
        double val1 = Double.parseDouble(one.getText());
        double val2 = Double.parseDouble(two.getText());
        //  i = (Integer.parseInt(one.getText())+Integer.parseInt(two.getText()));

        if (event.getSource() == add) {
            result = val1 + val2;
        } else {
            result = val1 - val2;
        }

        showResult.setText(String.valueOf(result));
    }
}