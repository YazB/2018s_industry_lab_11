package ictgradschool.industry.lab.swing.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener,
                                                            KeyListener {
private List<Balloon> balloons;
//    private  Balloon balloon;
    private Timer t;
    int numberOfBalloons;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);
        balloons=new ArrayList <Balloon>();
        numberOfBalloons=(int)((Math.random()*100)+1);
        for (int i = 0; i <numberOfBalloons ; i++) {
            balloons.add(new Balloon((int)((Math.random()*800)),(int)((Math.random()*800))));
        }
//        this.balloon = new Balloon(30, 60);
        this.addKeyListener(this);
        t=new Timer(50, this);

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
        t.start();
        if(e.getKeyCode()==KeyEvent.VK_UP) {
//            balloon.setDirection(Direction.Up);
            for (int i = 0; i < balloons.size(); i++) {
                balloons.get(i).setDirection(Direction.Up);
            }
        } if(e.getKeyCode()== KeyEvent.VK_DOWN) {
            for (int i = 0; i < balloons.size(); i++) {
                balloons.get(i).setDirection(Direction.Down);
            }
//            balloon.setDirection(Direction.Down);
        } if(e.getKeyCode()==KeyEvent.VK_LEFT) {
            for (int i = 0; i < balloons.size(); i++) {
                balloons.get(i).setDirection(Direction.Left);
            }
//            balloon.setDirection(Direction.Left);
        } if(e.getKeyCode()==KeyEvent.VK_RIGHT) {
            for (int i = 0; i < balloons.size(); i++) {
                balloons.get(i).setDirection(Direction.Right);
            }
//            balloon.setDirection(Direction.Right);
        } if(e.getKeyCode()==KeyEvent.VK_S){
            t.stop();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }


    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Put in new balloons here

        for (int i = 0; i < balloons.size(); i++) {
            balloons.get(i).move();
        }

        //Add new balloon here

        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
        @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //balloon.draw(g);
        for (int i = 0; i < balloons.size(); i++) {
           balloons.get(i).draw(g);
        }
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }
}